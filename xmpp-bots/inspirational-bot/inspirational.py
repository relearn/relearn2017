#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# le RMS XMPP bot
# usage: relearn.py [-h] -j JID -p PASSWORD -m MUC
# for example: $ python relearn.py -j username@domain.org -p entrepot -m room@muc.domain.org

# before running this script: 
# $ pip install argparse logging sleekxmpp pyasn1 pyasn1_modules
import argparse, json, logging, sleekxmpp
import random
import urllib2
import re
import json

# command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-j", "--jid", help="jabber identifier", type=str, required=True)
parser.add_argument("-p", "--password", help="password", type=str, required=True)
parser.add_argument("-m", "--muc", help="destination muc", type=str, required=True)
args = parser.parse_args()

# la Relearn tool introduction bot
class Relearn(sleekxmpp.ClientXMPP):

	def __init__(self, jid, password, room, nick):
		sleekxmpp.ClientXMPP.__init__(self, jid, password)

		self.room = room
		self.nick = nick

		self.add_event_handler("session_start", self.start) 
		self.add_event_handler("groupchat_message", self.inspirational)

	def start(self, event):
		self.get_roster()
		self.send_presence()
		self.plugin['xep_0045'].joinMUC(self.room, self.nick)

		# XEP-0084 User Avatar 
		# Requires SleekXMPP 81b7b2c1908e0f6a5435ce67745b5f4dafb59816

		with open('pg.jpg', 'rb') as avatar_file:
			avatar = avatar_file.read()
		avatar_id = self['xep_0084'].generate_id(avatar)
		info = {
			'id': avatar_id,
			'type': 'image/jpeg',
			'bytes': len(avatar)
			}
		self['xep_0084'].publish_avatar(avatar)
		self['xep_0084'].publish_avatar_metadata(items=[info])

		# XEP-0153: vCard-Based Avatars
		# Not working ATM

		#self['xep_0153'].set_avatar(avatar=avatar, mtype='image/png')

	def inspirational(self, msg):

		# this takes all the quotes into a pad and uses them as responses to a message
		pads = [
			'http://192.168.73.188/etherdump/not-so-utopian_open_source_pedagogies.raw.txt',
			'http://192.168.73.188/etherdump/versioned_networked_archive.raw.txt',
			'http://192.168.73.188/etherdump/Ransom-Headlessnesss-in-the-Design.raw.txt',
			'http://192.168.73.188/etherdump/collective-care.raw.txt',
			'http://192.168.73.188/etherdump/collective-care-clone2.raw.txt',
			'http://192.168.73.188/etherdump/collective-care-clone3.raw.txt',
			'http://192.168.73.188/etherdump/collective-care-clone4.raw.txt',
		]
		triggers_url = 'http://192.168.73.188/etherdump/bot-script-triggers.raw.txt'
		target_url = random.choice(pads)
		interactions = []
		responses = []
		triggers = []

		for line in urllib2.urlopen(triggers_url):
			line = line.replace('\n','')
			triggers.append(line)

		for line in urllib2.urlopen(target_url):
			finds = re.findall(r'"([^"]*)"', line)
			for find in finds:
				responses.append('"' + find + '"')

		response = random.choice(responses)
		interactions = [{'triggers': triggers, 'response': response}]

		for comment in interactions:
			triggers = comment['triggers']
			if msg['mucnick'] != self.nick and any(x in msg['body'].lower() for x in triggers):
				self.send_message(mto=msg['from'].bare, mbody=comment['response'], mtype='groupchat')

if __name__ == '__main__':

	logging.basicConfig(level=logging.INFO,
		format='%(levelname)-8s %(message)s')

	client = Relearn(args.jid, args.password, args.muc, "inspirational")

	client.register_plugin('xep_0045')
	client.register_plugin('xep_0030')
	client.register_plugin('xep_0084')
	#client.register_plugin('xep_0153')

	if client.connect():
		client.process(block=True)
	else:
		print("Can't connect.")