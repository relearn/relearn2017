#!/usr/bin/env python

import json
from urllib import urlencode
from urllib2 import urlopen#, HTTPError, URLError

# settings
settingsFile='settings.json'
with open(settingsFile) as f:
    settingsJson = json.load(f)
    apiurl = settingsJson.get("apiurl")
    apikey = settingsJson.get("apikey")
print(apiurl)
print(apikey)



padID = 'versioned_networked_archive'
#padID = 'tools'
#padID = 'complex.local'
#padID = 'reroam'
#padID = 'revisions-diff-testing'
#padID = 'testdiff'

# request
#request='listAllPads'
#data = {}
#data['apikey'] = apikey
#requestUrl = apiurl+request+'?'+urlencode(data)
#print(requestUrl)
#requestResults = json.load(urlopen(requestUrl))
#print(requestResults)
 
#request
#request='getRevisionsCount'
#data = {}
#data['apikey'] = apikey
#data['padID'] = padID
#requestUrl = apiurl+request+'?'+urlencode(data)
#print(requestUrl)
#requestResults = json.load(urlopen(requestUrl))
#print(requestResults)


# request
request='getRevisionsCount'
data = {}
data['apikey'] = apikey
data['padID'] = padID
requestUrl = apiurl+request+'?'+urlencode(data)
print(requestUrl)
requestResults = json.load(urlopen(requestUrl))
print(requestResults)

revisionsCount = requestResults['data']['revisions']
print(revisionsCount)


i = 1
while i < revisionsCount-100:
#for i in range(0, revisionsCount):
    # request
    request='createDiffHTML'
    data = {}
    data['apikey'] = apikey
    data['padID'] = padID
    data['startRev'] = i
    data['endRev'] = i+1
    requestUrl = apiurl+request+'?'+urlencode(data)
    print(requestUrl)
    requestResults = json.load(urlopen(requestUrl))
    print(requestResults)
    html = requestResults['data']['html']
    #f = open("diff.html","w")
    #f.write(html)
    #f.close()
    with open("diff.html", "a") as myfile:
        myfile.write(html)
    i += 100
