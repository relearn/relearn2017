#!/bin/bash



# startingLine = halfLines - size/2
# echo "\n" till startingLine
# startingChar = cols - size/2
# echo " " startingChar

clear

# config
size=24
repeat=100

# math
maxSize=$(($size*4))
cols=$(tput cols)
lines=$(tput lines)
halfCols=$(($cols/2))
halfLines=$(($lines/2))

# main loop
f=0
while [ "$f" -lt "$repeat" ]
do

# loop e
e=4
while [ "$e" -lt "$maxSize" ]
do
    clear
    actualSize=$e
    echo $actualSize
    # loop d break to starting line
    d=0
    while [ "$d" -lt "$(($halfLines-$actualSize/2/2))" ]
    do
	echo -ne "\n" 
	d=$(($d + 1))
    done
    # loop b lines
    b=0
    while [ "$b" -lt "$(($actualSize/2))" ] 
    do
	# loop c break to starting char
	c=0
	while [ "$c" -lt "$(($halfCols-$actualSize/2))" ]
	do
	    echo -ne " " 
	    c=$(($c + 1))
	done
	# loop a char
	a=0
	while [ "$a" -lt "$actualSize" ]
	do
	    echo -ne "#" 
	    a=$(($a + 1))
	done
	echo -ne "\n" 
	b=$(($b + 1))
    done
    e=$(($e + 4))
    sleep 0.01
done


# loop e
e=$maxSize
while [ "$e" -gt "4" ]
do
    clear
    actualSize=$e
    echo $actualSize
    # loop d break to starting line
    d=0
    while [ "$d" -lt "$(($halfLines-$actualSize/2/2))" ]
    do
	echo -ne "\n" 
	d=$(($d + 1))
    done
    # loop b lines
    b=0
    while [ "$b" -lt "$(($actualSize/2))" ] 
    do
	# loop c break to starting char
	c=0
	while [ "$c" -lt "$(($halfCols-$actualSize/2))" ]
	do
	    echo -ne " " 
	    c=$(($c + 1))
	done
	# loop a char
	a=0
	while [ "$a" -lt "$actualSize" ]
	do
	    echo -ne "#" 
	    a=$(($a + 1))
	done
	echo -ne "\n" 
	b=$(($b + 1))
    done
    e=$(($e - 4))
    sleep 0.01
done


done
