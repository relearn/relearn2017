#!/bin/bash

urlsSource=("http://osp.kitchen:9999/p/relearn-2017-CCTF" 
            "http://osp.kitchen:9999/p/relearn-2017-CCTF-2"
            "http://osp.kitchen:9999/p/relearn-2017-CCTF-3"
            "http://osp.kitchen:9999/p/relearn-2017-CCTF-4")

urlsDestination=(   "http://192.168.73.188:9001/p/collective-care-clone" 
                    "http://192.168.73.188:9001/p/collective-care-clone2"
                    "http://192.168.73.188:9001/p/collective-care-clone3"
                    "http://192.168.73.188:9001/p/collective-care-clone4")

# use while loop (with nohup)
# or cronjob */5 * * * * path/padclone.sh
for i in ${!urlsSource[@]}
do
    urlExportTxt=${urlsSource[$i]}/export/txt
    urlImport=${urlsDestination[$i]}/import
    echo
    echo $urlExportTxt "---->" $urlImport
    echo
    
    # stolen from http://192.168.73.188:9001/p/Ransom-Headlessnesss-in-the-Design
    wget -qO- $urlExportTxt | curl -F "file=@-;type=text/plain" $urlImport
done

