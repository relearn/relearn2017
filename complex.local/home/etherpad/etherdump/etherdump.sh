#!/bin/bash
# PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games
cd /home/etherpad/etherdump
etherdump pull --all --pub .
etherdump index *.meta.json > index.html
