Relearn 2017

Unibash README

Unibash is an experiment with bash, unicode and etherpad.
It's essentially a collection of bash scripts, with single-glyph names, using unicode icons as meaningful (?) names.
Providing commands to download content from a pad (↓), upload content to a pad (↑) or excute as script from a pad (☁). Essentially allowing for the pads to be used as stdin & stdout.

TODO: use a convention for stdin & stdout, perhaps stdpad?

Installing unibash:
mkdir unibash && cd unibash
curl http://192.168.73.188:9001/p/unibash-install/export/txt > install.sh
curl http://192.168.73.188:9001/p/unibash-uninstall/export/txt > uninstall.sh
chmod +x ./install.sh uninstall.sh
curl http://192.168.73.188:9001/p/unibash-readme/export/txt > README
sudo ./install.sh

Testing it, The following command should produce Hello, world!:
☁ hello-world


Available commands:
↓ [padname] (unicodepoint 2193) || DOWN
Downloads the content from the given padname and sends it to stdout
	
	↓ test | less
	
↑ [padname] (unicodepoint 2191) || UP
Takes the content from stdin and uploads it to the given pad
	
	echo "Hello, world" | ↑ unibash-example

☁ [padname] (unicodepoint 2601) || EXPAD
Executes the code on the given padname. Be carefull! there are no safety checks. Please read the code on the pad before you execute it!

	☁ hello-world
	
🌩 [binname] [padname] || EXPADBIN
Send the content of the pad as a file to the given binary.  Be carefull! there are no safety checks. Please read the code on the pad before you execute it!

    🌩 node hello-world-node
	
🗊 [padname] (unicodepoint 1f5ca) || PAD
Convenience function. Transform a name into a pad-url
	
	🗊 hello-world
	
Examples

Download content of a pad. Sort it with local function, upload again:
	↓ test | sort | ↑ test-sorted


Download content of a pad. Sort it with the sort function on the pads, upload again:
	↓ test | ☁ sort | ↑ test-sorted

