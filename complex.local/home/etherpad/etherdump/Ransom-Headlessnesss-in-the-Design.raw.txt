Headless Design:
    http://osp.kitchen:9999/p/relearn-2017-RHitDoL

Thomas had originally proposed this track
There was a generation that started to work with GUI's. Now there is a group of designers that work from a coding background.
the other side of the interface, the command line being the accessibility of this alternate side 
Without icons pr metaphors.
interesting design just by using commandline
The command line as a central tool in a design practice.
Largely underdevelopped, not so understood, could be helpful for experimentations in non direct timelines
The result is 'delayed' instead of instant gratification in the process of designing. One has to wait for the script to run, which is a text based process, rather than a visual.
Text based interfaces, text based parameters
You need to speak the language of the text interface. Design & typography is very closely related, and now, with the command line as a central tool, even more.
Umberto Eco: the Macintosh was introducing a 'catholic' approach, rather than the protestant of the previous cli -like interfaces.
Catholic: icons, images, (iconisation of abstract notions) to push this parallel -> the Mac
Protestant: interpretation of the bible is left to the reader, the believer.  -> the CLI
This metaphor does still apply now with the GUI & command line. 
the Bible was written in Latin, making it less accessible to a general audience, who then had to rely on the images present in the book, whereas protestantism is more focused on the individual interpretation of text
-> what could be an "interface" to the Bible narrative is the stained glass of the churches with the representations of the biblic scenes, that made the story accessible to a even bigger amount of people includig the non readers ones

Will this juxtaposition 'end in' a Beeldenstorm? (what are the implications)
Designing without icons. What does that mean? Does it change your practise?
UNIX as it's own religion?
A number of maxims that have gained currency among the builders and users of UNIX
Can these maxims be applied as a sort of design style book?

- make each program do one thing well
- expect the output of a program to become the input of the next
- 
- 
source: unix timesharing system maxims IBM (HAL)
Researching the resistance of tools which are not usually used by designers
Can the concept of "piping" be translated?

Umberto Eco
http://relearn2017-headless.surge.sh/

'Formalise' maxims for headless design. "Can one pipe one design into another?"

https://ia902701.us.archive.org/BookReader/BookReaderImages.php?zip=/12/items/bstj57-6-1899/bstj57-6-1899_jp2.zip&file=bstj57-6-1899_jp2/bstj57-6-1899_0003.jp2&scale=1&rotate=0
Unix Time-Sharing System Forward, Doug McIlroy, Bell Laboratories, 1978, p. 1902 


Language of Design:
Marcel proposed this side of the track originally.
    Design more and more starting from/fricting with software & technological background. Ie. going from Photoshop to HTML.
	expressing designs, friction between the showing and the doing
Big difference between GUI, and html generated projects.
	Codeability of designs.
Saphir Whorf hypothesis: once you start thinking in a certain language, it limits the thoughts you have.
Linguistic relativity https://en.wikipedia.org/wiki/Linguistic_relativity
	
Find a different way to design the design-languages
Try to examine how coding influences design?
For example: css has lots of hierarchies
How does the structure i write, connect to what happens on the screen. And see how you then can use that to change your design from a linguistic approach.
Eg. Can we descibe an HTML generated design into language. And translate that back into a visual image.

---

Is there any affective relationship with the shell?
What do we mean with the shell?
The borders are blurry. 
The word shell comes from the fact that it brings multiple programs together.

Working in a gui sometimes feels like working as a blind man, not knowing what happens, and being focused on the outcomes.
Working in the shell is much more language based. It needs consious thinking and combining logical steps to create the outcome.

This way of working is relatively new for the graphic design field.
How does this shift connect to the tradition of graphic design.

There is also a sense of 'purity' when writing code yourself. But then there are many more steps to take.
- object oriented programming languages, libraries
- low level programming languages
- assembly
- etc

Abstracting away design concepts and reusing elements done by other people-

Linking back to a tradition of using instructions to create designs

Designing Dictionaries
https://ss64.com/bash/

Hybrid representations
https://cryptpad.fr/slide/#/1/edit/SDkmi12jIYRHEAzK-EpmPA/kwX400Oo3RmiLiXpu7j9q81J/


Open structures
http://openstructures.net/


-------------------------------------------
Decomposing the design process
Non-linearity
Tree representation or not ?


Using "Unix Time-sharing system forward" text as a model to start form some ideas ?


grammar of the design process
related to the Sapir–Whorf Hypothesis ? (could be a starting point as well)
what would be the litteracy of coding?


What’s my color? rose pale

How do tools affect the way we design?
tº•ls

"Instead of counting quantities (1,2,3,4...), the Pirahã only estimate quantities (relatively small, relatively large)"
http://www.personal.psu.edu/ejp10/blogs/thinking/2008/07/the-language-without-numbers.html

140 cross-browser supported color names
https://www.w3schools.com/cssref/css_colors.asp

The accent is the language?
take in account the possible interpretations that people can have from a language (oral language = accent…)

Hacker = Command line = Fear

tty

wtb
http://www.revue-backoffice.com/numeros/01-faire-avec/eric-schrijver-culture-hacker-peur-wysiwyg
http://i.liketightpants.net/and/hackers-culture-and-the-fear-of-wysiwyg

stereotypes

Markdown = Code?

GUI generates code
Markdown editor works in reverse

Standardization

from etymology (standard): 
1 - Falling within an accepted range of size, amount, power, quality, etc.
...
3 - Having recognized excellence or authority. 

Who get's to decide what is the standard
CSS color names: https://www.w3schools.com/colors/colors_names.asp
"Natural color system" (primary colors) palette: https://en.wikipedia.org/wiki/Natural_Color_System
Who get to write the web ? http://i.liketightpants.net/and/who-makes-standards

https://en.wikipedia.org/wiki/Tincture_(heraldry)
Heraldic representation of colors
http://www.paulcox.centrepompidou.fr/article-270040.html
Paul Cox's book made of visual patterns refering to colors

There is a paradox in the code editors; code is supposed to lead to interfaces but the code is already an interface in itself because of its highlighting syntax system
---> Is there a way to use code without visual appearance ?

Tim Ingold, in "A brief history of lines" describes the evolution of writing text and points out the fact that written text was first written without any spaces and then litlte by little became "composed", with indentations, paragraphs breaks etc for readability reasons  https://taskscape.files.wordpress.com/2011/03/lines-a-brief-history.pdf

Atom supported languages
https://en.wikipedia.org/wiki/Atom_(text_editor)

TOOLS TO PRODUCE IMAGES
Image code to produce image/image as a code : https://affinelayer.com/pixsrv/
Literaldraw by OSP : http://osp.kitchen/tools/literaldraw/
Google quick draw https://quickdraw.withgoogle.com/
metafont (curves are pre-defined)
inkscape (xml editor - also, there is spiro)
graphviz (dot (basic, oriented graph !) or neato ? (+command line -> unoriented graph))

CHARACTERS/GLYPHS
List of unicode characters https://en.wikipedia.org/wiki/List_of_Unicode_characters

Code and GUI are just two representations of the same abstact structure.

Move from Graphics to Language
And Move from Language to Graphics

Image as language
https://nl.wikipedia.org/wiki/Piet_(programmeertaal)
http://www.abstractbrowsing.net/

(Notations for dance movements) >> http://www.phantomranch.net/folkdanc/images/joc_de_zestre_notation.gif  /// https://s-media-cache-ak0.pinimg.com/originals/5b/35/a4/5b35a4ce33f7e84d69d64bd5a336abca.png
http://www.cs.dartmouth.edu/~mckeeman/gifs/dance1.gif

Headless art
Sol Lewitt

Analyzing the instruction models? cooking recipes? Code is a manifesto of your design

Get a text from a pad in the terminal:
wget -qO- http://192.168.73.188:9001/p/[[padname]]/export/txt

Recipe to display the contents of this pad in less:
wget -qO- http://192.168.73.188:9001/p/Ransom-Headlessnesss-in-the-Design/export/txt | less

Sort the sentences:
wget -qO- http://192.168.73.188:9001/p/Ransom-Headlessnesss-in-the-Design/export/txt | sort | less

export & import | quine | https://en.wikipedia.org/wiki/Quine_%28computing%29
http://192.168.73.188:9001/p/terminal-import/

wget -qO- http://192.168.73.188:9001/p/Ransom-Headlessnesss-in-the-Design/export/txt | sort | curl -F "file=@-;type=text/plain" http://192.168.73.188:9001/p/Ransom-Headlessnesss-in-the-Design-sorted/import

Prepend the content on a pad with ' . '
while true; do wget -qO- http://192.168.73.188:9001/p/headless-spacing/export/txt | (printf " . " && cat) | curl -F "file=@-;type=text/plain" http://192.168.73.188:9001/p/headless-spacing/import; sleep 0.5; done;

Stubborn pad: replace more with less
while true; do wget -qO- http://192.168.73.188:9001/p/more-or-less/export/txt | sed 's/more/less/' | curl -F "file=@-;type=text/plain" http://192.168.73.188:9001/p/more-or-less/import; sleep 1; done;

Exploding pad: replace gnu with gnu is not unix
while true; do wget -qO- http://192.168.73.188:9001/p/gnu-is-not-exploding/export/txt | sed 's/gnu/gnu is not unix/' | curl -F "file=@-;type=text/plain" http://192.168.73.188:9001/p/gnu-is-not-exploding/import; sleep 1; done;

Sort the html:
wget -qO- http://complex.local:9001/p/Ransom-Headlessnesss-in-the-Design/export/html | sort | curl -F "file=@-;type=text/plain" http://192.168.73.188:9001/p/Ransom-Headlessnesss-in-the-Design-sorted-html/import

Sort the html, get and print to pdf
wget -qO- http://complex.local:9001/p/Ransom-Headlessnesss-in-the-Design/export/html | sort | curl -F "file=@-;type=text/plain" http://192.168.73.188:9001/p/Ransom-Headlessnesss-in-the-Design-sorted-html/import && wget -qO- http://complex.local:9001/p/Ransom-Headlessnesss-in-the-Design-sorted-html/export/html | node stdin.js sorted-html.pdf

Sort the text, get and print to pdf
wget -qO- http://complex.local:9001/p/Ransom-Headlessnesss-in-the-Design/export/txt | sort | curl -F "file=@-;type=text/plain" http://192.168.73.188:9001/p/Ransom-Headlessnesss-in-the-Design-sorted-txt/import && wget -qO- http://complex.local:9001/p/Ransom-Headlessnesss-in-the-Design-sorted-txt/export/html | node stdin.js sorted-txt.pdf


Mapping the mornig session with graphviz (tree) and then representing it in another way to display the translateral links between elements.
Aaaarhg graphviz trouble here
So graphviz is probably not the proper language to use

|

Trying to find a logic to make layout design with using block unicode characters,
And at the same time trying to make a comparison between the logics of wisiwig and inline/coded design from the designer's point of view
And also re reading https://monoskop.org/images/a/a6/Ludovico,_Alessandro_-_Post-Digital_Print._The_Mutation_of_Publishing_Since_1894.pdf and finding some nice ideas for the elaboration of this comparison
https://monoskop.org/images/5/55/Frutiger_Adrian_Signs_and_Symbols_Their_Design_and_Meaning.pdf
▀ ▁ ▂ ▃ ▄ ▅ ▆ ▇ █ ▉▊▋▌▍▎▏▐ ░ ▒ ▓ ▔ ▕ ▖▗ ▘▙ ▚ ▛ ▜▝ ▞ ▟ ▤ ▥ ▦ ▧ ▨ ▩ ◰ ◱ ◲ ◳

|

- Converting the NYT front page text to 91 different programming languages and combining them into one pdf.

- A circle in bash
http://complex.local:9001/p/headless-circle-in-bash
- Grepping the terms input, output and design in the UNIX Time-Sharing journal from 1978
http://complex.local:9001/p/headless-grepping-unix-time-sharing
- Designing heraldic equivalents for every single css color name (all 140 of them) 

|

Piet program is a program executed by setting a grid of colors that can be translated into a text
1- Trying to reverse this process
2- Pick an image (Lenna)
3- Using the standards of the css colors designation to re-create this image with words
4- Caculate the occurence of words to resume the image

--> synesthesy interpretation of colours (numbers, days of the week, months)

here is the list of the color values of the image pixel by pixel :
http://complex.local/headless-design-publish/headless-design/quentin2/txt_lena.txt
obtained with Imagemagick :
    convert lena.jpg txt_lena.txt

how to find the closer name reference of a color value :
http://chir.ag/projects/name-that-color/
script :
http://complex.local/headless-design-publish/headless-design/quentin2/whatcolor.js

this script is refering to a list of references coming from different sources :
    - Wikipedia (https://en.wikipedia.org/wiki/Lists_of_colors)
    - Crayola (https://en.wikipedia.org/wiki/List_of_Crayola_crayon_colors)
    - Resene (a Color-Name Dictionary : http://people.csail.mit.edu/jaffer/Color/Dictionaries.html#Introduction)

translation of the previous list turning color values into color names :
http://complex.local/headless-design-publish/headless-design/quentin2/lennanew-transformed.txt

Same with a lower definition :
    1 - http://complex.local/headless-design-publish/headless-design/quentin2/lenna2new.txt
    2 - http://complex.local/headless-design-publish/headless-design/quentin2/lenna2new-transformed.txt
    Occurence:
    3 - http://complex.local/headless-design-publish/headless-design/quentin2/occurencelennacolors.txt
    
PAD SCRIPTS :
    http://192.168.73.188:9001/p/headless-whatcolor
    http://192.168.73.188:9001/p/headless-colorlist

|

convert svg format files to a unicode-based description thanks to a set of variables :
    ─ ━│┃┄┅┆┇┈┉┊┋┌┍┎┏

|

Mapping different ways of representing a layout
Relationships, Fields, Grids, Naming systems, Hierarchies, Areas, Scales, Colors
Separation of concerns
Pointing and refering

Mapping as structures https://u4493183.dl.dropboxusercontent.com/u/4493183/mapping_new_york_times.jpg
Mapping into (natural language) language https://u4493183.dl.dropboxusercontent.com/u/4493183/mapping%20in%20text.jpg
Original http://i.huffpost.com/gen/1823270/original.jpg


command line to clone git repo from server
git clone relearn@192.168.73.188:/var/www/html/headless-design/
password: entrepot

---------------------

Brother hl-1430 driver (sort of)
https://www.openprinting.org/ppd-o-matic.php?driver=hl1250&printer=Brother-HL-1430&show=1
Or if that fails: http://www.brotherdriver.com/brother-hl-1430-driver/

Highlight
Syntax highlighter
http://www.andre-simon.de/doku/highlight/en/install.php

----------------------

Producing a manifesto :
    - of intentions ?
    - of usage ?
    - (both ?)
    - recipe ?

examples :
https://ia902701.us.archive.org/BookReader/BookReaderImages.php?zip=/12/items/bstj57-6-1899/bstj57-6-1899_jp2.zip&file=bstj57-6-1899_jp2/bstj57-6-1899_0003.jp2&scale=1&rotate=0
https://conditionaldesign.org/manifesto/

Houghlines
http://complex.local:9001/p/headless-houghlines

---------------------

I.
1. Design or build operating systems
2. make each programm do one thing well
3. except the output of every program to become the input to another, as yet unknow program

II.
1. All first input shall come from the tracks pad [simply because we felt we had grasped enough idea to start experimenting]
2. Method to obtain an output should be chosen by the operator, using only headless methods (?) [thus, creating a new standard]
3. The result shall become the new input of another operator [applying is own standard to the previous one]

┌——> standard 0 ——> standard 1 ——> standard 2 ——> standard 3 ——> standard 4 ——> standard 5 ——┐
└——<————————<————————<———————<———————<———————<———————┘
[as we were originally 5 participants]


Conditional design manifesto :
"Through the influence of the media and technology on our world, our lives are increasingly characterized by speed and constant change. We live in a dynamic, data-driven society that is continually sparking new forms of human interaction and social contexts. Instead of romanticizing the past, we want to adapt our way of working to coincide with these developments, and we want our work to reflect the here and now. We want to embrace the complexity of this landscape, deliver insight into it and show both its beauty and its shortcomings.
Our work focuses on processes rather than products: things that adapt to their environment, emphasize change and show difference.
Instead of operating under the terms of Graphic Design, Interaction Design, Media Art or Sound Design, we want to introduce Conditional Design as a term that refers to our approach rather than our chosen media. We conduct our activities using the methods of philosophers, engineers, inventors and mystics."

use a substitution method such as in : http://f-u-t-u-r-e.org/r/37_Edward_Said_Joe_Scanlan_Classisme_une_introduction_FR.md

Headless design manifesto
"Through the influence of the graphic user interface of our softwares on our daily usage, our lives are increasingly characterized by an incomprehension toward our softwares. We live in a usage-only-oriented society that is continually sparking new user-friendly interfaces. Instead of being mere users, we want to adapt our way of working to coincide with these developments, and we want our work to reflect our tools. We want to embrace the complexity , deliver insight into it and show both its beauty and its shortcomings.
Our tracks focuses on intricated headless processes : understand a design process through command-line usage where  the output of every program to become the input to another, as yet unknow program. Instead of operating under any GUI, we want to introduce Randsom Headlessness in the design as a term that refers to our approach rather than our finnality. We conduct our activities using the power of the command-line tool"

				┌—————————┐
				│   input - standard    ▕————┐
				└—————————┘               │ use to 
				   │                A                                │ translate 
	  interprete │                │ depend on            │
				  │                │                        dictionnary 
				  v                │                                  │
		      language  <——————————┘
				  │                      color-code            
				  │  
				  v   
		 vision - interpretation 

Unibash

Unibash is an experiment with bash, unicode and etherpad.
It's essentially a collection of bash scripts, with single-glyph names, using unicode icons as meaningful (?) names.
Providing commands to download content from a pad (↓), upload content to a pad (↑) or execute as script from a pad (☁). Essentially allowing for the pads to be used as stdin & stdout.

http://192.168.73.188:9001/p/unibash-readme

Download pad: http://192.168.73.188:9001/p/%E2%86%91
Upload pad: http://192.168.73.188:9001/p/%E2%86%93
Execute code on a pad: http://192.168.73.188:9001/p/%E2%98%81
Transform name into pad url: http://192.168.73.188:9001/p/%F0%9F%97%8A

Recipe to download list of colors, find closest css-colors and send it back to the pad:
↓ headless-colorlist | 🌩 node headless-whatcolor | ↑ stdpad


