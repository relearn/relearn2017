#!/bin/bash

clear

# config
sleeptime=0.02
size=10
repeat=100

# math
cols=$(tput cols)
half=$(($cols/2))
debutNoSpace=$(($half-$size))
debut=$(($debutNoSpace/2))

# action
i=$size
a=0
while [ "$a" -lt $repeat ] 
do
    b=0    
    while [ "$b" -lt "$i" ] 
    do
    b=$(($b + 1))
	d=0
	while [ "$d" -lt "$debut" ]
	do
	    echo -ne "\\ "
	    d=$(($d + 1))
	done
	c=0
	while [ "$c" -lt "$b" ]
	do
	    echo -ne "\\ "
	    c=$(($c + 1))
	done
	echo -ne "\n"
	sleep $sleeptime
    done
    while [ "$b" -gt 0 ] 
    do
	d=0
	while [ "$d" -lt "$debut" ]
	do
	    echo -ne "/ "
	    d=$(($d + 1))
	done
        c=0
        while [ "$c" -lt "$b" ]
        do
            echo -ne "/ "
            c=$(($c + 1))
        done
	sleep $sleeptime
    echo -ne "\n"
    b=$(($b - 1))
    done
   a=$(($a + 1))
done
