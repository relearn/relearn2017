#! /usr/bin/env bash

houghlines () {
  (( ${#@} < 2 )) && echo -e "\n\thoughlines\tthreshold beeldbestand\n" && exit 1
  threshold="$1" && bestand="$2"
  convert "${bestand}" -median 3 \( +clone -colorspace Gray -canny 0x1+10%+25% -write "${bestand%.*}_canny-edges.png" -background none -fill black -stroke lime -strokewidth 1 -hough-lines 9x9+"$threshold" -write "${bestand%.*}_block-lines.mvg" -write "${bestand%.*}_block-lines.png" \) -composite "${bestand%.*}_hough-lines_$( printf "%03d" "$threshold" ).png"
  open "${bestand%.*}_hough-lines_$( printf "%03d" "$threshold" ).png"
  open "${bestand%.*}_block-lines.png"
}

houghlines "${@}"

