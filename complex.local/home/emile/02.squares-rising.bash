#!/bin/bash

clear

# config
sleeptime=0.02
repeat=1000
size=10

# math
cols=$(tput cols)
lines=$(tput lines)
range=$(($cols-$size))


# action
a=0
while [ "$a" -lt "$repeat" ] 
do
    number=$RANDOM
    let "number %= $range"

    b=0
    while [ "$b" -lt $(($size)) ] 
    do
	c=0
	while [ "$c" -lt "$number" ] 
	do
	    echo -ne " "
	    c=$(($c + 1))
	done
	d=0
	while [ "$d" -lt "$size" ] 
	do
	    echo -ne "#"
	    d=$(($d + 1))
	done
    echo -ne "\n"
    sleep $sleeptime
    b=$(($b + 1))
    done

    a=$(($a + 1))
done
echo
