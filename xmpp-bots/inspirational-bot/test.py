# this takes all the quotes into a pad and uses them as responses to a message
import urllib2
import re
import json
import random
import os

pads = [
	'http://192.168.73.188/etherdump/not-so-utopian_open_source_pedagogies.raw.txt',
	'http://192.168.73.188/etherdump/versioned_networked_archive.raw.txt',
	'http://192.168.73.188/etherdump/Ransom-Headlessnesss-in-the-Design.raw.txt',
	'http://192.168.73.188/etherdump/collective-care.raw.txt',
	'http://192.168.73.188/etherdump/collective-care-clone2.raw.txt',
	'http://192.168.73.188/etherdump/collective-care-clone3.raw.txt',
	'http://192.168.73.188/etherdump/collective-care-clone4.raw.txt',
]


# this takes all the quotes into a pad and uses them as responses to a message
triggers_url = 'http://192.168.73.188/etherdump/bot-script-triggers.raw.txt'
target_url = random.choice(pads)
interactions = []
responses = []
triggers = []

for line in urllib2.urlopen(triggers_url):
	line = line.replace('\n','')
	triggers.append(line)

for line in urllib2.urlopen(target_url):
	finds = re.findall(r'"([^"]*)"', line)
	for find in finds:
		responses.append('"' + find + '"')

response = random.choice(responses)

interactions = [{'triggers': triggers, 'response': response}]
# print interactions

for comment in interactions:
	triggers = comment['triggers']
	# print triggers